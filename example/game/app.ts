import { Realm, Msg, Model } from '../../src/realm'
import { v } from '../../src/dom'
import { Map } from 'immutable'
import { fromEvent, interval } from 'rxjs'
import { map, filter } from 'rxjs/operators';

$(() => {
    let realm = new Realm();

    const [UP, DOWN, LEFT, RIGHT] = [0, 1, 2, 3];

    let snakeMap: number[][] = [];

    let end = false;

    const [EMPTY_CELL, SCORE_CELL] = ['·', '$']

    function createMap(N: number, M : number): string[][] {
        let result: string[][] = [];
        for (let i = 0; i < N; ++i) {
            if (!result[i]) result[i] = [];
            for(let j = 0; j < M; ++j)
                if (Math.floor(Math.random() * 5) === 1)
                    result[i][j] = SCORE_CELL;
                else
                    result[i][j] = EMPTY_CELL;
        }
        return result;
    }

    // -1: original direction
    // others: see below
    function initializeTurningPoints(N: number, M: number): number[][] {
        let result: number[][] = [];
        for (let i = 0; i < N; ++i) {
            if (!result[i]) result[i] = [];
            for (let j = 0; j < M; ++j)
                result[i][j] = -1;
        }
        return result;
    }

    function getOpposite(dir: number): number {
        return dir ^ 1;
    }

    function getCharFromDir(dir: number): string {
        switch (dir) {
            case 0: return '^';
            case 1: return 'v';
            case 2: return '<';
            case 3: return '>';
            default: return '×';
        }
    }

    function nextPos(x: number, y: number, dir: number): [number, number] {
        let result: [number, number];
        switch (dir) {
            case UP: result = [x - 1, y]; break;
            case DOWN: result = [x + 1, y]; break;
            case LEFT: result = [x, y - 1]; break;
            case RIGHT: result = [x, y + 1]; break;
            default: result = [x, y];
        }
        return result;
    }

    function gameOver() {
        alert('Game Over :P');
        end = true;
        window.location.reload(true);
    }

    function drawSnake(map: string[][], turns: number[][], 
        x: number, y: number, length: number, dir: number): string {
        let result = '';
        let newMap = JSON.parse(JSON.stringify(map));
        let step = 0;
        do {
            step += 1;
            if (newMap[x] && newMap[x][y] && newMap[x][y] !== EMPTY_CELL && newMap[x][y] !== SCORE_CELL) {
                console.log(newMap[x][y]);
                return '' + gameOver();
            }
            newMap[x][y] = getCharFromDir(dir);
            length -= 1;
            if (turns[x][y] !== -1 && step == 1 
                && newMap[x] && newMap[x][y] && newMap[x][y] !== EMPTY_CELL 
                && newMap[x][y] !== SCORE_CELL) {
                console.log("case 2");
                console.log(newMap[x][y]);
                return '' + gameOver();
            }
            if (turns[x][y] !== -1) {
                dir = getOpposite(turns[x][y]);
                if (length === 1) {
                    turns[x][y] = -1;
                }
            }
            let nP = nextPos(x, y, getOpposite(dir));
            [x, y] = nP;
        } while (length > 0);
        for (let i = 0; i < newMap.length; ++i) {
            for (let j = 0; j < newMap[i].length; ++j) {
                result += '    ' + newMap[i][j] + '    ';
            }
            result += '\n';
        }
        return result;
    }

    function validate(x: number, y: number, mx: number, my: number): void {
        if (x >= mx || x < 0 || y >= my || y < 0) {
            gameOver();
        }
    }

    function getUpdLength(map: string[][], x: number, y: number) {
        if (map[x][y] === SCORE_CELL) {
            map[x][y] = EMPTY_CELL;
            return 1;
        }
        return 0;
    }

    function resolveData(N: number, M: number, x: number, y: number, dir: number
        , length: number, score: number, map: string[][], model: Model) {
        validate(x, y, N, M);
        let delta = getUpdLength(map, x, y);
        score += delta;
        length += delta;
        return model.merge({
            posX: x,
            posY: y,
            direction: dir,
            length: length,
            map: map,
            score: score
        });
    }

    realm.registerMsg({
        UP: [],
        DOWN: [],
        LEFT: [],
        RIGHT: [],
        MOVE: [],
    });

    // 0 up
    // 1 down
    // 2 left
    // 3 right
    realm.registerModel({
        length: 1,
        direction: Math.floor(Math.random() * 4),
        N: 40,
        M: 60,
        map: createMap(40, 60),
        turnMap: initializeTurningPoints(40, 60),
        posX: 20,
        posY: 20,
        score: 0
    });

    realm.registerUpdate(model => action => {
        let dir = model.get('direction');
        let turns = model.get('turnMap');
        let length = model.get('length');
        let x = model.get('posX');
        let y = model.get('posY');
        let N = model.get('N');
        let M = model.get('M');
        let map = model.get('map');
        let score = model.get('score');
        return action.case({
            UP: () => {
                if (dir === UP)
                    [x, y] = nextPos(x, y, dir);
                else if (dir === LEFT || dir === RIGHT) {
                    turns[x][y] = getOpposite(dir);
                    dir = UP;
                    [x, y] = nextPos(x, y, dir);
                }
                return resolveData(N, M, x, y, dir, length, score, map, model);
            },
            DOWN: () => {
                if (dir === DOWN)
                    [x, y] = nextPos(x, y, dir);
                else if (dir === LEFT || dir === RIGHT) {
                    turns[x][y] = getOpposite(dir);
                    dir = DOWN;
                    [x, y] = nextPos(x, y, dir);
                }
                return resolveData(N, M, x, y, dir, length, score, map, model);
            },
            LEFT: () => {
                if (dir === LEFT)
                    [x, y] = nextPos(x, y, dir);
                else {
                    if (dir === UP || dir === DOWN) {
                        turns[x][y] = getOpposite(dir);
                        dir = LEFT;
                        [x, y] = nextPos(x, y, dir);
                    }
                }
                return resolveData(N, M, x, y, dir, length, score, map, model);
            },
            RIGHT: () => {
                if (dir === RIGHT)
                    [x, y] = nextPos(x, y, dir);
                else {
                    if (dir === UP || dir === DOWN) {
                        turns[x][y] = getOpposite(dir);
                        dir = RIGHT;
                        [x, y] = nextPos(x, y, dir);
                    }
                }
                return resolveData(N, M, x, y, dir, length, score, map, model);
            },
            MOVE: () => {
                [x, y] = nextPos(x, y, dir);
                return resolveData(N, M, x, y, dir, length, score, map, model);
            },
            _: () => model
        });
    });

    realm.registerMainView((model, msgs) => 
        v('div', {}, {align: 'center'}, [
            ...(drawSnake(model.get('map'),
                model.get('turnMap'),
                model.get('posX'),
                model.get('posY'),
                model.get('length'),
                model.get('direction')).split('\n')
                .map(line => v('div', [v('tt', {}, { style: { 'font-family' : '"Courier New", Courier, monospace'}}, line), v('br')]))),
            v('input', { onClick: [msgs.UP] }, { type: 'button', value: '↑'}, []),
            v('input', { onClick: [msgs.DOWN] }, { type: 'button', value: '↓'}, []),
            v('input', { onClick: [msgs.LEFT] }, { type: 'button', value: '←'}, []),
            v('input', { onClick: [msgs.RIGHT] }, { type: 'button', value: '→'}, []),
            v('div', [
                v('p', model.get('score'))
            ])
        ])
    );

    realm.listenToMsgStream(msgType => fromEvent(document, 'keydown').pipe(filter(ev => !end)).pipe(map((ev: Event) => {
        let msg: Msg;
        ev.preventDefault();
        switch ((ev as KeyboardEvent).which) {
            case 38: msg = msgType.UP as Msg; break;
            case 40: msg = msgType.DOWN as Msg; break;
            case 37: msg = msgType.LEFT as Msg; break;
            case 39: msg = msgType.RIGHT as Msg; break;
            default: msg = msgType.UP as Msg; break;
        }
        return msg;
    })));

    alert('Close me to Start');

    realm.listenToMsgStream(msgType => interval(200).pipe(filter((e) => !end)).pipe(map((x) => {
        return msgType.MOVE as Msg;
    })));


    realm.startOn('main');
})