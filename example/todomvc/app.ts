import { Realm, v } from '../../src/realm'
import { MsgConstructorWithArgs, MsgType } from '../../src/msgtype'
import { StringIndexed } from '../../src/helpers'
import { List } from 'immutable'
import { msgHandler } from './msghandler';

$(() => {
    let realm = new Realm();

    function focus(oldVnode: any, vnode: any) {
        if (oldVnode.data.class.editing === false &&
            vnode.data.class.editing === true) {
            vnode.elm.querySelector('input.edit').focus();
        }
    }

    /** 
    *  Register the model to store todos
    * - todos :: [{title :: string, id :: number, done :: bool, editing :: bool}]
    *       title: the content of the todo
    *       id: the numberial id of todos
    *       done: if it is marked as completed
    *       editing: if it is switched to editing mode
    * - view :: string, can take 'all' | 'completed' | 'active'. a todo filter mode
    * - current_title :: string, current_text that is being edited
    * - next_id :: number, id for next todo
    * */
    realm.registerModel({
        todos: List([]),
        view: 'all',
        current_title: '',
        next_id: 0
    });

    /**
     *  Register Messages
     *      - UpdateTitle:     Takes a string `str` and change `current_title` to `str`
     *      - Create:          Use `current_title` to create a new todo and add to the list
     *      - ToggleAll:       if there are todos that is not marked as completed, 
     *                         then mark all those todos as completed
     *                         else mark all todos as not completed. 
     *      - ToggleDone:      Takes an id and flip the completed state of the todo that has the given id.
     *      - ToggleEditing:   Takes an id and fliip the editing state of the todo that has the given id.
     *      - Remove:          Takes an id and remove the todo that has the given id.
     *      - RemoveAll:       Remove all todos from the list
     *      - RemoveCompleted: Remove todos that are marked as completed
     *      - ChangeViewType:  Takes a string and change the todo filter mode to the given string
     * */
    realm.registerMsg({
        UpdateTitle: [String],
        Create: [],
        ToggleAll: [],
        ToggleDone: [Number],
        ToggleEditing: [Number],
        Remove: [Number],
        RemoveAll: [],
        RemoveCompleted: [],
        ChangeViewType: [String]
    });

    realm.registerUpdate(model => action => action.case(msgHandler(model)));

    /**
     * The View template of a todo item.
     *      - attr: attributes of the item (passed from upstream view, main view)
     *      - events: the event handlers passed from upstream view
     *      - msgs: registered messages
     *  */
    realm.registerView('my-todo', (attr: StringIndexed, events: StringIndexed, msgs: MsgType) => {
        return v('li', {}, {
                class : {completed: attr.done, editing: attr.editing},
                key: attr.id
        }, [
            v('div.view', [
                v('input.toggle', { onClick: [msgs.ToggleDone, attr.id] }, 
                                  {type: 'checkbox', checked: attr.done, id: `toggle-${attr.id}`}, []),
                v('label', { onDblClick: [msgs.ToggleEditing, attr.id] }, {}, attr.title),
                v('button.destroy', { onClick: [msgs.Remove, attr.id] }, {}, []),
            ]),
            v('input.edit', {
                onKeyDown: [(ev: any) => {
                if (ev.keyCode === 13)
                    ev.target.blur();
                }],
                onBlur: [msgs.ToggleEditing, attr.id],
                onInput: [(e: any) => (msgs.UpdateTitle as MsgConstructorWithArgs)(e.target.value)]
            }, {value: attr.title}, [])
        ])
    })

    /**
     * The main view of todo app
     *  */
    realm.registerMainView((model, msgs) => {
        let cond = model.get('view');
        let f: (x: any) => boolean;
        let length = model.get('todos').size;
        let hasItem = length > 0;
        if (cond === 'all') f = (x: any) => true;
        else if (cond === 'active') f = (x: any) => !x.done;
        else f = (x: any) => x.done;
        let todos = model.get('todos').filter(f) as List<any>;
        let viewType = model.get('view');
        return v('section.todoapp', [
            v('h1', 'todos'),
            v('input.new-todo', {
                onInput: [(e: any) => (msgs.UpdateTitle as MsgConstructorWithArgs)(e.target.value)],
                onKeydown: [(ev: any) => {
                    if (ev.keyCode === 13) {
                        ev.target.value = '';
                        return msgs.Create
                    }
                }] 
            }, {placeholder: 'What need to be done?'}, []),
            v('section.main', {}, {
                style: { display: hasItem ? 'block' : 'none' }
            },[
                    v('input.toggle-all', {}, { type: 'checkbox', id: 'toggle-all' }, []),
                    v('label', {onClick: [msgs.ToggleAll]  }, { for: 'toggle-all'}, []),
                    v('ul.todo-list', [
                        ...(todos.map((todo: any) => v('my-todo', {}, todo, [])).toArray())
                    ]),
                ]),
            v('footer.footer', {}, { style: { display: hasItem ? 'block' : 'none' }}, [
                v('span.todo-count', [v('strong', length + ` item${length === 1 ? '' : 's'} left`)]),
                    v('ul.filters', [
                        v('li', [v('a', { onClick: [msgs.ChangeViewType, 'all'] }, { class: { selected: viewType === 'all' } }, 'All')]),
                        v('li', [v('a', { onClick: [msgs.ChangeViewType, 'active'] }, { class: { selected: viewType === 'active' } }, 'Active')]),
                        v('li', [v('a', { onClick: [msgs.ChangeViewType, 'completed'] }, { class: { selected: viewType === 'completed'} }, 'Completed')]),
                    ]),
                    v('button.clear-completed', {onClick: [msgs.RemoveCompleted]}, {}, 'Clear completed'),
                ])
        ])
    });

    realm.startOn('app');
});