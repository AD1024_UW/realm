#!/bin/bash

echo "Compiling"
tsc ./*.ts
browserify ./app.js | uglifyjs > ./bundle.js
# browserify ./app.js -o ./bundle.js