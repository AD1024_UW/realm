import { Model } from "../../src/realm";
import { List } from "immutable";

function getItemPos(todos: List<any>, id: number): number {
    for (let i = 0; i < todos.size; ++i) {
        if (todos.get(i).id === id) return i;
    }
    return 0;
}

let msgHandler = (model: Model) => {
    let todos = model.get('todos') as List<any>;
    return {
        UpdateTitle: (newTitle: string) => model.merge({ current_title: newTitle }),
        Create: () => {
            if (model.get('current_title').length > 0) {
                let id = model.get('next_id');
                return model.merge({
                    // Todos is an immutable List
                    todos: todos.push({
                        title: model.get('current_title'),
                        id: id,
                        done: false,
                        editing: false,
                    }), next_id: id + 1, current_title: ''
                });
            } else {
                return model;
            }
        },
        ToggleEditing: (id: number) => {
            let pos = getItemPos(todos, id);
            let item = todos.get(pos);
            item.editing = !item.editing;
            let newTodos: List<any> = todos;
            if (!item.editing) {
                let newTitle = model.get('current_title');
                if (newTitle.length > 0) {
                    item.title = newTitle;
                    newTodos = todos.slice(0, pos).concat([item], todos.slice(pos + 1));
                } else {
                    newTodos = todos;
                }
            }
            return model.merge({ todos: newTodos });
        },
        ToggleDone: (id: number) => {
            let pos = getItemPos(todos, id);
            let item = todos.get(pos);
            item.done = !item.done;
            return model.merge({
                todos: todos.slice(0, pos).concat([item], todos.slice(pos + 1))
            });
        },
        ToggleAll: () => {
            let opedCnt = todos.filter((x: any) => x.done).size;
            if (opedCnt === todos.size || opedCnt === 0) {
                todos = todos.map((x: any) => {
                    x.done = !x.done;
                    return x;
                })
            } else {
                todos = todos.map((x: any) => {
                    if (!x.done) x.done = true;
                    return x;
                })
            }
            return model.merge({ todos: todos });
        },
        Remove: (id: number) => {
            let rmPos = getItemPos(todos, id);
            return model.merge({
                todos: todos.slice(0, rmPos).concat(todos.slice(rmPos + 1))
            });
        },
        RemoveCompleted: () => {
            return model.merge({ todos: todos.filter((x) => !x.done) });
        },
        ChangeViewType: (view: string) => {
            return model.merge({ view: view });
        },
        _: () => model
    }
}

export { msgHandler }