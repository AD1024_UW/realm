import { Realm, v, Msg, Model } from '../../src/realm'
import { Observable, fromEvent, interval } from 'rxjs'
import { map, throttle, debounceTime, distinctUntilChanged } from 'rxjs/operators'
import { MsgConstructorWithArgs } from '../../src/msgtype';

$(() => {
    function WIKIPEDIAGET(s: string, cb: Function) {
        $.ajax({
            url: "https://en.wikipedia.org/w/api.php?format=json",
            dataType: "jsonp",
            jsonp: "callback",
            data: {
                action: "opensearch",
                search: s,
                namespace: 0,
                limit: 10,
            },
            success: function (data) { cb(data[1]); },
        });
    }

    let realm = new Realm();
    
    realm.registerModel({
        results: []
    });

    realm.registerMsg({
        TriggerSearch: [String],
    })

    realm.registerUpdate(model => action => action.case({
        TriggerSearch: (str) => {
            console.log(str);
            let modelStream = Observable.create((observer: any) => {
                WIKIPEDIAGET(str, (data: any) => observer.next(data));
            }).pipe(map((lst: any) => (model: Model) => lst === undefined ? 
                    model.merge({results: []}) : model.merge({ results: lst })));
            return modelStream;
        }
    }));

    realm.registerMainView((model, msgs) => 
        v('div', [
            v('p', {}, {}, [
                v('p', "Search Wiki:"), 
                v('input', {}, {placeholder: 'Search here', id: 'wiki-input'}, [])]),
            v('ul', {}, {}, model.get('results').map((res: any) => v('li', res)))
        ])
    );

    realm.listenToMsgStream(
        msgs =>
            fromEvent($('#wiki-input'), 'keydown')
                .pipe(debounceTime(150))
                .pipe(distinctUntilChanged())
                .pipe(map((ev: any) => {
                    return (realm.getMsgType().TriggerSearch as MsgConstructorWithArgs)(ev.target.value)
                }))
    );

    realm.startOn('main');
})