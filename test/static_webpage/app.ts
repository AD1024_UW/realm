import { Realm, v} from '../../src/realm';

$(() => {
    let realm = new Realm();
    realm.registerMainView((model, msgs) => v('div', [
            v('p', "Hello"),
            v('h3', "Title Size 3"),
            v('ul', [1, 2, 3, 4].map(it => v('li', '' + it))
            )]));

    realm.startOn("main");
});