import { Realm, VDOM } from '../../src/realm'
import { v } from '../../src/dom'

$(() => {
    let realm = new Realm()

    const links = [
        "https://github.com/AD1024",
        "https://github.com/zyh3838438zyh",
        "https://uw.edu",
        "https://ad1024.space"
    ]

    realm.registerMainView((model, msgs) => 
        v('div', [
            v('my-list', links.map(li => v('my-link', li)))
        ])
    );

    realm.registerView('my-list', (attr, events, children, msgs) => 
        v('div', [
            v('h1', 'List of Links'),
            v('div', {}, { id: 'list-div' }, 
                v('ul', (children as VDOM[]).map(child => v('my-list-item', child))))
        ])
    );

    realm.registerView('my-list-item', (attr, events, children, msgs) => 
        v('li',{}, {style: {'font-size': '25pt'}}, children as VDOM)
    )

    realm.registerView('my-link', (attr, events, children, msgs) =>
        v('a', {}, {href: (children as string)}, children as string)
    );

    realm.startOn('main')
});