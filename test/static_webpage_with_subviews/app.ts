import { Realm, View, v, VDOM } from "../../src/realm"
import { Map } from "immutable"

$(() => {
    let realm = new Realm();

    realm.registerModel({});

    realm.registerMainView((model, msgs) =>
        v('div', [
            v('h1', 'Static Webpage w/ SubView'),
            v('my-content', {}, {},
            [1, 2, 3, 4].map(datum => v('li', `Item Children ${datum}`)))
        ])
    );

    realm.registerView('my-content', (attr, events, children, msgs) => 
        v('ul', [
            v('li', "Item 1"),
            v('li', "Item 2"),
            v('li', "Item 3"),
            ...(children as VDOM[])
        ]));
    
    realm.startOn('main');
})