import { Realm, View, v } from "../../src/realm"
import { Map } from "immutable"

$(() => {
    let realm = new Realm();

    realm.registerModel({
        counter: 0,
    });

    realm.registerMainView((model, msgtype) => {
        return v("div", [
            v("h1", "A simple counter"),
            v("h2", {}, {}, model.get('counter')),
            v("input", { onClick: [msgtype.Increment, 1] }, { type: "button", value: "+1" }, []),
            v("br"),
            v("input", { onClick: [msgtype.Decrement, 1] }, { type: "button", value: "-1" }, []),
        ])
    });

    realm.registerMsg({
        Increment: [Number],
        Decrement: [Number],
    });

    realm.registerUpdate(model => action => action.case({
        Increment: (x: any) => Map({ counter: model.get('counter') + 1 }),
        Decrement: (x: any) => Map({ counter: model.get('counter') - 1 }),
        _: () => model,
    }));

    realm.startOn("main");
});