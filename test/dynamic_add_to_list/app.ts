import { Realm } from '../../src/realm'
import { v } from '../../src/dom'
import { MsgConstructorWithArgs } from '../../src/msgtype';

$(() => {
    let realm = new Realm();

    realm.registerModel({
        note_list: ['a', 'b'],
        current_text: '',
    });

    realm.registerMsg({
        UPDATE_STRING: [String],
        APPEND: []
    });

    realm.registerUpdate(model => action => action.case({
        UPDATE_STRING: (str) => model.merge({current_text: str}),
        APPEND: () => {
            let lst = model.get('note_list');
            lst.push(model.get('current_text'));
            return model.merge({note_list: lst});
        },
        _ : () => model,
    }));

    realm.registerMainView((model, msgs) => 
        v('div', [
            v('ul', model.get('note_list').map(
                    (x: string) => v('li', x))),
            v('textarea', {onChange: [(e: any) => (msgs.UPDATE_STRING as MsgConstructorWithArgs)(e.target.value)]}, {}, []),
            v('br'),
            v('input', {onClick: [msgs.APPEND, model.get('current_text')]}, {type: 'button', value: 'add'}, [])
    ]));

    realm.startOn('main');
});