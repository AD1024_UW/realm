#!/bin/bash

compile() {
    echo "Compiling $1"
    tsc ./test/$1/*.ts
    browserify ./test/$1/app.js | uglifyjs > ./test/$1/bundle.js
    # browserify ./test/$1/app.js -o ./test/$1/bundle.js
}

compile "static_webpage"

test1="dynamic_webpage_without_side_effects"
compile ${test1}
browserify ./test/${test1}/app.js -o ./test/${test1}/bundle.js

test2="static_webpage_with_subviews"
compile ${test2}

compile "static_webpage_with_multiple_subviews"

compile "dynamic_webpage_with_stream"

compile "game"

compile "dynamic_add_to_list"

compile "wikisearch"
