import { StringIndexed } from "./helpers";
let Type = require("union-type");

export interface Msg {
    _name: string,
    case: <T>(cases: StringIndexed<(...params: any[]) => T>) => T,
    _isMsg: boolean,
};
export interface MsgConstructorWithArgs {
    (...params: any[]): Msg,
    _isMsg: boolean,
    case: (cases: StringIndexed) => any,
};
export type MsgConstructor = Msg | MsgConstructorWithArgs;
export type MsgType = StringIndexed<MsgConstructor>;

export function createMsgType(msgs: StringIndexed): MsgType {
    let msgType: MsgType = {}; // hack on the type of "this"
    let type = Type(msgs);
    Object.keys(msgs).forEach(name => {
        msgType[name] = type[name] as MsgConstructor;
        msgType[name]._isMsg = true;
    });
    msgType.case = type.case;
    return msgType;
}
 
