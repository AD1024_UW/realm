export function unwrap<T>(v: T | undefined | null, errorMessage: string): T {
    if (typeof v === "undefined" || v === null) {
        throw errorMessage;
    } else {
        return v;
    }
}

export type StringIndexed<V = any> = {[key: string]: V};