// import MsgType from "./msgtype"
import { h } from "snabbdom/h"
import { VNode } from "snabbdom/vnode";
import { StringIndexed } from "./helpers";
import { View, SubView } from "./realm";

export function v(tagName: string): VDOM;
export function v(tagName: string, children: string | VDOM | VDOM[]): VDOM;
export function v(tagName: string, events: any,
    attrs: any, children: string | VDOM | VDOM[]): VDOM;
export function v(tagName: any, e?: any, attr?: any, c?: any): VDOM {
    if (attr === undefined && c === undefined && e !== undefined) {
        return new VDOM(tagName, undefined, undefined, e);
    }
    return new VDOM(tagName, e, attr, c);
}

export class VDOM {

    private tagName: string;
    private events: StringIndexed;
    private attrs: StringIndexed;
    private children: any;
    
    constructor(tagName: string, events: any,
        attrs: any, children: string | VDOM | VDOM[]) {
            this.tagName = tagName;
            if (events !== undefined) {
                for (let e in events) {
                    if (e.indexOf('on') !== 0) {
                        throw "Illegal Event: " + e;
                    }
                }
            }
            this.events = events;
            this.attrs = attrs;
            this.children = children;
    }

    render(wrapper: any, meta: any): VNode {
        let options: any = {on: {}, props: {}};
        if (this.events !== undefined) {
            for (let e in this.events) {
                let event = this.events[e][0];
                let params = this.events[e].slice(1);
                options.on[e.toLowerCase().substring(2)] = [wrapper(event), ...params]; // inject evils to the emitters
            }
        }
        if (this.attrs !== undefined) {
            for (let attr in this.attrs) {
                if (attr === 'style') {
                    options['style'] = this.attrs[attr];
                } else if (attr === 'class') {
                    options['class'] = this.attrs[attr];
                } else {
                    options.props[attr] = this.attrs[attr];
                }
            }
        }

        // Check whether it is a registered SubView
        let subViewMap = meta.subViewMap;
        if (subViewMap !== undefined && this.tagName in subViewMap) {
            let subView: SubView = subViewMap[this.tagName];
            if (subView.length === 3) {
                return ((subView as (...param: any[]) => VDOM)
                        (this.attrs, this.events, meta.msgs)).render(wrapper, meta);
            } else {
                return (subView as (...param: any[]) => VDOM)
                    (this.attrs, this.events, this.children, meta.msgs).render(wrapper, meta);
            }
        }

        // Render VDOM to VNode
        if (this.children !== undefined) {
            if (this.children instanceof VDOM) {
                this.children = this.children as VDOM;
                return h(this.tagName, options, this.children.render(wrapper, meta));
            } else if (this.children instanceof Array) {
                return h(this.tagName, options, this.children.map(child => {
                    if (child)
                        return child.render(wrapper, meta)
                    return null;
                }));
            } else {
                return h(this.tagName, options, this.children.toString());
            }
        }
        return h(this.tagName, options);
    }
}