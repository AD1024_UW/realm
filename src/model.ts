import { Map } from "immutable";
import { StringIndexed } from "./helpers";

export class Model implements StringIndexed {
    private modelData: Map<string, any>;

    constructor(model: any) {
        this.modelData = Map(model);
    }
    
}

