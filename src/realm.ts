import { VDOM, v } from "./dom"
// import { Model } from "./model";
import { Msg, MsgConstructor, MsgType, createMsgType, MsgConstructorWithArgs } from "./msgtype";

import { unwrap, StringIndexed } from "./helpers";

import { init } from "snabbdom";
import { VNode } from "snabbdom/vnode";
import { classModule } from "snabbdom/modules/class";
import { propsModule } from "snabbdom/modules/props";
import { eventListenersModule } from "snabbdom/modules/eventlisteners";
import { styleModule } from "snabbdom/modules/style";
import { Observable, merge } from "rxjs"
import { scan, startWith, map, concatMap, share } from "rxjs/operators"
import { Map, update } from "immutable";

type Model = Map<string, any>;
type View = (model: Model, msgs: MsgType) => VDOM;
type SubView = ((attr: StringIndexed, events: StringIndexed, children: VDOM | VDOM[] | string, msgs: MsgType) => VDOM)
    | ((attr: StringIndexed, events: StringIndexed, msgs: MsgType) => VDOM);
type Update = (model: Model) => (action: Msg) => (Model | Observable<(model: Model) => Model>);

type FilterUpdateFunc = (model: Model) => (...args: any[]) => (Model | Observable<(model: Model) => Model>);

type UpdateStream = {
    filterType: (msg: Msg | string, func: FilterUpdateFunc) => UpdateStream
};
type ModelTransitionStream = Observable<(model: Model) => Model>;
type alienStreamFunc = (msgs: StringIndexed<MsgConstructor>) => Observable<Msg>;

class Realm {
    private patch: (oldVnode: Element | VNode, vnode: VNode) => VNode;
    private initModel: Model = Map();
    private msgType: MsgType = {};
    private DOM?: Element | VNode;
    private msgStream?: Observable<Msg>;
    private modelStream?: Observable<Model>;
    private viewStream?: Observable<VNode>;
    private mainUpdate: Update = (model: Model) => _ => model;
    private mainView?: View;
    private msgEmitter?: { next: any };
    private subViewMap?: StringIndexed;
    private updates: StringIndexed<FilterUpdateFunc[]> = {};
    private alienStreams: alienStreamFunc[] = [];

    constructor() {
        this.patch = init([
            styleModule,
            classModule,
            eventListenersModule,
            propsModule
        ]);
    }

    registerModel(model: StringIndexed) {
        this.initModel = Map(model);
    }

    registerMsg(msgs: StringIndexed) {
        this.msgType = createMsgType(msgs);
    }

    registerMainView(view: View) {
        if (this.mainView) {
            throw "main view has been registered.";
        } else {
            this.mainView = view;
        }
    }

    registerUpdate(update: Update) {
        this.mainUpdate = update;
    }

    updateStream(): UpdateStream {
        return this;
    }

    filterType(msg: Msg | string, func: FilterUpdateFunc): UpdateStream {
        let index = typeof msg === "string" ? msg : msg._name;
        if (this.updates[index] === undefined) {
            this.updates[index] = [];
        }
        this.updates[index].push(func);
        return this;
    }

    eventEmitterWrapper = (event: MsgConstructor | Function): Function => {
        let updateAndPropogate = (msg: Msg) => {
            if (this.msgEmitter && msg !== undefined) this.msgEmitter.next(msg);
        };
        if ((event as MsgConstructor)._isMsg) {
            return (...args: any[]) => {
                let msg: Msg;
                try {
                    msg = (event as MsgConstructorWithArgs)(...(args.slice(0, args.length - 2)));
                } catch (e) {
                    msg = event as Msg;
                }
                updateAndPropogate(msg);
            }
        } else {
            return (...args: any[]) => {
                let msg = (event as Function)(...args)
                updateAndPropogate(msg);
            }
        }
    }

    registerView(name: string, view: SubView) {
        if (this.subViewMap === undefined) {
            this.subViewMap = {};
        }
        this.subViewMap[name] = view;
    }

    private update(model: Model, msg: Msg) {
        function mOrUStoUS(modelOrUpdateStream: Model | ModelTransitionStream): ModelTransitionStream {
            if (modelOrUpdateStream instanceof Observable) {
                return modelOrUpdateStream;
            } else {
                return Observable.create((e: { next: Function }) => {
                    e.next((model: Model) => modelOrUpdateStream);
                });
            }
        }

        let updateStream: ModelTransitionStream = Observable.create();
        if (this.mainUpdate) {
            let stream = mOrUStoUS(this.mainUpdate(model)(msg));
            updateStream = merge(updateStream, stream);
            // modelStream = modelStream.pipe(concatMap((model: Model): Observable<Model> => {
            //     let newModel: Model | Observable<Model> = this.mainUpdate(model)(msg);
            //     return newModel instanceof Observable ? newModel
            //         : Observable.create((e: { next: Function }) => e.next(newModel))
            // }));
        }
        if (this.updates[msg._name] !== undefined) {
            updateStream = this.updates[msg._name].reduce(
                (updateStream: ModelTransitionStream, update: FilterUpdateFunc): ModelTransitionStream => {
                    let cases: StringIndexed<(params: any[]) => Model | ModelTransitionStream> = {};
                    cases[msg._name] = (...args: any[]) => update(model)(...args);
                    let stream = mOrUStoUS(msg.case(cases));
                    return merge(updateStream, stream);
                }, updateStream);
        }

        // let modelStream: Observable<Model> = Observable.create(
        //     (e: { next: Function }) => e.next(model));

        return updateStream;
    }

    sendMsg(msg: Msg) {
        if (this.msgEmitter) {
            this.msgEmitter.next(msg);
        }
    }

    listenToMsgStream(msgs2Stream: alienStreamFunc) {
        this.alienStreams.push(msgs2Stream);
    }

    getMsgType() {
        return Object.freeze(this.msgType);
    }

    startOn(id: string) {
        let DOM = document.getElementById(id);
        if (DOM != null) this.DOM = DOM;

        let modelEmitter: { next: Function } = { next: () => { } };
        let model: Model;

        this.modelStream = Observable.create((e: { next: Function }) => {
            modelEmitter = e;
        }).pipe(share()) as Observable<Model>; // key hack here

        this.modelStream.subscribe(_model => model = _model);

        this.msgStream = Observable.create((e: { next: any }) => this.msgEmitter = e) as Observable<Msg>;
        this.msgStream.subscribe((msg: Msg) => {
            let modelTransitionStream = this.update(model, msg);
            modelTransitionStream.subscribe((modelTransit => {
                modelEmitter.next(modelTransit(model));
            }));
        });

        this.viewStream = this.modelStream
            .pipe(map((model?) => {
                let mainView = unwrap(this.mainView, "main view does not exist.");
                let msgType = this.msgType;
                return mainView(model, msgType);
            })).pipe(map(DOM => DOM.render(this.eventEmitterWrapper, {
                subViewMap: this.subViewMap,
                msgs: this.msgType
            })));

        this.viewStream.subscribe(view => {
            let DOM = unwrap(this.DOM, "DOM specified does not exist.");
            this.patch(DOM, view);
            this.DOM = view;
        });

        modelEmitter.next(this.initModel);

        for (let func of this.alienStreams) {
            func(this.getMsgType()).subscribe((ev) => {
                if (this.msgEmitter) {
                    this.msgEmitter.next(ev)
                }
            });
        }
    }
}

export {
    Realm,
    Model,
    View,
    SubView,
    Update,
    VDOM,
    Msg, MsgConstructor, MsgType,
    v,
    FilterUpdateFunc,
    UpdateStream
};