# Realm

CSE 402 Spring 2019 Final Project

# 5.1 Pre-Proposal
[Pre-proposal](https://docs.google.com/document/d/19BTn5iTwoBqg9ekY2sq1upuFPGzDJ8lVD4LbnFnEs5w/edit#heading=h.ho24qwxbjg9z) (A totally different proposal)

# 5.2 Proposal
[Realm Proposal](https://docs.google.com/document/d/1gd87bRiM8S7MS8mU_JSVenAwSv-KbLTFbE5FwwTDRd8/edit?usp=sharing)

# 5.3 Design
[Design](https://docs.google.com/document/d/1bAcoFZGHjdzA6x-IRoFMsRtCAf60HkfXMrcOJkEo2_Q/edit?usp=sharing)

# 5.4 Report
[Realm Report](https://docs.google.com/document/d/1-0SXZ1zryL2XzDijYDZQjVkJZSFsmtZozPCZT8IlvY4/edit?usp=sharing)

## Demo Video
[Demo Video](https://bitbucket.org/AD1024_UW/realm/raw/fb7c17ffa74b98ff6a323b1e2f90c14f1617d9da/Demo_Video.mov)

## Poster

[Poster Slides](https://docs.google.com/presentation/d/1c5Ia_jeZ87EWw2DNeiUH4e7EKZnSUMdY/edit#slide=id.p1)

# How to use?

## Get Started

First, clone this repository, and run `npm install` in the repo directory. If you want to build tests and the example, make sure that you have [tsc](https://www.npmjs.com/package/typescript), [browserify](https://www.npmjs.com/package/browserify) and [uglifyjs](https://www.npmjs.com/package/uglify-js) installed.

## First Step

To use Realm to manage your webpage, first create the entry script `app.ts` and an `index.html` that includes [jQuery](https://jquery.com/), `app.js` (which is the file after compiling `app.ts`), your CSS files and **a HTML element (such as a `div`) that has an id** which will be used to attach the virtual DOM while rendering.

In `app.ts`, first instantiate your Realm object and register `Model`, `Msg` and `View` to it.

```typescript
$(() => {
    let realm = new Realm();
    
    realm.resgiterModel({/* Your Model */});
    realm.resgiterMsg({/* Your Msg */});
    realm.registerUpdate(model => action => action.case({/* Update behaviors */}));
    realm.registerMainView((model, msgs) => v(/* your virtual dom */));

    realm.startOn('<id to the host element>');
});
```

### Model
The model of Realm essentially is an `object` (or a Key-Value map). It will take the object and create an immutable Map.
```typescript
    realm.registerModel({
        count: 0
        // Maybe more fields
    });
```

### Msg
Msg is kept as a Union Type, which supports functional programming using pattern matching. For instance, if a Msg can be constructed without using an argument, we can simply write:
```typescript
    realm.resgiterMsg({
        MsgWithoutArg: []
    });
```

If a msg can be constructed iff **certain argument(s) with speificied type(s)** is/are provided, we can write:
```typescript
    realm.resgisterMsg({
        MsgWithArgs: [A, B ... ] // where A, B ... are types (Number, String, etc.)
    });
```

### Update
Update takes `model` and an `action` to perform pattern matching on `action` and (probably) update 
the model in the action. To match an `action`(Msg), we use `action.case({})`:

```typescript
    realm.registerUpdate(model => action => action.case({
        MsgWithoutArg: () => {/* something that resturns a model or a stream of models */},
        MsgWithArgs: (arg1, arg2 ...) => {/* something that resturns a model or a stream of models */},
        _: () => {/* something that resturns a model or a stream of models */} // this is the case that none of the action is matched
    }))
```

### View / MainView
The view of Realm is actually a Virtual Dom Tree. You can use the function `v` in `dom.ts` to create a virtual dom tree (Node). 
```typescript
    realm.resgiterMainView((model, msgs) => 
        v(htmlTag, events, attributes, children)
    )
```
where *children* can be a string or an array of virtual dom tree (node).

## Detailed Tutorial Example
See *Appendix 2* in [5.3 Design](https://docs.google.com/document/d/1bAcoFZGHjdzA6x-IRoFMsRtCAf60HkfXMrcOJkEo2_Q/edit?usp=sharing)